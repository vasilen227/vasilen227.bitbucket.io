<?php
session_start();
if ($_SESSION['key'] == 27){
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <title>Biology</title>
</head>
<body>

<div style="text-align:center; width: 100%; margin-top: 3%;" class="container">
  <img  style="width:45%;" src="img/logo.pdf" class="rounded" alt="Cinque Terre">
</div>

<div style="width:40%; margin-left:30%;" class="conatiner">

<button name = "subjects" style="width:100%; margin-left: 0%; margin-top: 7%; " type="submit" class="btn btn-success" onclick="window.open('./subjects.php','_self')" >Home</button>

<button name = "all" style="width:23%; margin-top: 7%; float: left;" type="submit" class="btn btn-success" onclick="window.open('./all_biology.php','_self')" >All questions</button>

<form name="form" style="width:69%; margin-left: 8%; margin-top: 7%; float: right;" action="biology.php" method="post">
<div class="form-group">

<button name = "random" style="float:left; " type="submit" class="btn btn-success">Random 35 questions</button>

<input style="width: 16%; float: right;" class="form-control" id="email" name="go">
<button name = "submit" style="margin-left: 12%;" type="submit" class="btn btn-success">Go to</button>

</div>
</form>

<?php
if (isset($_POST["submit"])) {
$chosen_number = $_POST["go"];
	if (preg_match('/^\d+$/', $chosen_number)){
		if ($chosen_number > 0 && $chosen_number < 301){
			$_SESSION["chosen_number"] = $chosen_number;
			echo '<script language="javascript">window.open("custom_biology.php", "_self");</script>';
		}
		else {
		echo '<script language="javascript">';
  		echo 'alert("Please insert number between 1 and 300")';
  		echo '</script>';
		}
	}
	else {
		echo '<script language="javascript">';
  		echo 'alert("Please insert number between 1 and 300")';
  		echo '</script>';
	}
}

if (isset($_POST["random"])) {

$random_numbers = array();

for ($i = 0; $i < 35 ; $i++){
	$random_numbers[$i] = rand(1, 300);
}

$_SESSION["random_numbers"] = $random_numbers;
echo '<script language="javascript">window.open("random_biology.php", "_self");</script>';

}

?>

</div>
</body>
</html>
<?php
}
else {
echo '<html>
<h1> Forbidden ! </h1>
<p> Please <a href="login.php"> Log in </a> to your account, in order to access this directory. </p>
</body>
</html>';
}
?>