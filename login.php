<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <title>Login</title>
</head>
<body>

<div style="text-align:center; width: 100%; margin-top: 3%;" class="container">
  <img  style="width:45%;" src="img/logo.pdf" class="rounded" alt="Cinque Terre">
</div>

<div style="width:40%; margin-left:30%;" class="conatiner">

<form name="form" action="login.php" method="post">
  <div class="form-group">
    <label for="email">Username:</label>
    <input class="form-control" id="email" name="username">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" class="form-control" id="pwd" name="password">
  </div>
  <button name = "sign" style="width:100%;" type="submit" class="btn btn-success">Sign In</button>
</form>

</div>

<?php 

if (isset($_POST['sign'])) {

$username = $_POST['username'];
$password = $_POST['password'];


$usernames = file('usernames.txt');
$passwords = file('passwords.txt');

$line_number = false;

while (list($key, $line) = each($usernames) and !$line_number) {
   $line_number = (strpos($line, $username) !== FALSE) ? $key + 1 : $line_number;
}
if ($line_number>=1){
  $line_number += -1;

  if (strcmp(rtrim($passwords[$line_number],"\n"),strval($password)) == 0) {
    echo "<script>window.open('./subjects.php','_self')</script>";
    $_SESSION['key'] = 27;
  }
  else{
    echo '<script language="javascript">';
    echo 'alert("Wrong password, please try again!")';
    echo '</script>';
  }  
}
else{
  echo '<script language="javascript">';
    echo 'alert("Wrong username, please try again!")';
    echo '</script>';
}
}
?>

</body>
</html>